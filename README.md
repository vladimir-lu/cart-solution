## Solution summary

The solution to the exercise is split into two main components:

  * The domain model, which includes `Price`, `Article` and the functions in `Cart`
  * The input-facing `Shop` class

There is some extra complexity above the bare-bones solution in splitting it up like that, and in adding error handling, but both of these make 
the code clearer and more resilient in my opinion.

## Testing

There is reasonable unit test coverage for the happy path, on both the core functions and the actual end-to-end price input, however there are
two areas where additional coverage would be desirable (in a production environment):

  * Randomized testing to ensure that the functions are probably well behaved across all inputs (such as testing the buy one get one free offer always works)
  * I/O testing for the `Shop` class to simulate user input and display functionality

