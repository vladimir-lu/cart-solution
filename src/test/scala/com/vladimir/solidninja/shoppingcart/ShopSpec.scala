package com.vladimir.solidninja.shoppingcart

import org.scalatest.{Matchers, WordSpec}

import scala.util.Success

class ShopSpec extends WordSpec with Matchers {

  "A price" should {
    "format correctly in toString" in {
      Price(0).toString shouldBe "£0.00"
      Price(1).toString shouldBe "£0.01"
      Price(101).toString shouldBe "£1.01"
      Price(100).toString shouldBe "£1.00"
      Price(-20).toString shouldBe "£0.20"
    }
  }

  "An article" should {
    "be convertible from a string input preserving case" in {
      Article.unapply("Orange") shouldBe Some(Orange)
      Article.unapply("Apple") shouldBe Some(Apple)
    }

    "not be convertible when the casing is wrong" in {
      Article.unapply("orange") shouldBe None
      Article.unapply("apple") shouldBe None
    }

    "not be convertible from other strings" in {
      // NOTE: typically such a test should be written using randomized testing
      Article.unapply("Banana") shouldBe None
    }
  }

  "A cart" should {
    "price articles" in {
      Cart.priceOf(Orange) shouldBe Price(pence = 25)
      Cart.priceOf(Apple) shouldBe Price(pence = 60)
    }

    "compute the total price of the empty cart" in {
      Cart.checkoutTotal(Nil) shouldBe Price(0)
    }

    "compute the total price of items in the cart" in {
      Cart.checkoutTotal(Apple :: Nil) shouldBe Price(60)
      Cart.checkoutTotal(Apple :: Orange :: Nil) shouldBe Price(85)
      Cart.checkoutTotal(Apple :: Orange :: Apple :: Nil) shouldBe Price(85)
      Cart.checkoutTotal(Apple :: Apple :: Orange :: Apple :: Nil) shouldBe Price(145)
    }

    "calculate the buy one get one free offer correctly" in {
      Cart.buyOneGetOneFree(0) shouldBe 0
      Cart.buyOneGetOneFree(1) shouldBe 1
      Cart.buyOneGetOneFree(2) shouldBe 1
      Cart.buyOneGetOneFree(3) shouldBe 2
      Cart.buyOneGetOneFree(4) shouldBe 2
      Cart.buyOneGetOneFree(5) shouldBe 3
      Cart.buyOneGetOneFree(6) shouldBe 3
    }

    "calculate the three for the price of two offer correctly" in {
      Cart.threeForThePriceOfTwo(0) shouldBe 0
      Cart.threeForThePriceOfTwo(1) shouldBe 1
      Cart.threeForThePriceOfTwo(2) shouldBe 2
      Cart.threeForThePriceOfTwo(3) shouldBe 2
      Cart.threeForThePriceOfTwo(4) shouldBe 3
      Cart.threeForThePriceOfTwo(5) shouldBe 4
      Cart.threeForThePriceOfTwo(6) shouldBe 4
      Cart.threeForThePriceOfTwo(7) shouldBe 5
    }

    "have the buy one get one free offer for Apples" in {
      Cart.applyOffer(Apple, 1) shouldBe 1
      Cart.applyOffer(Apple, 2) shouldBe 1
      Cart.applyOffer(Apple, 3) shouldBe 2
    }

    "have the three for the price of two offer for Oranges" in {
      Cart.applyOffer(Orange, 1) shouldBe 1
      Cart.applyOffer(Orange, 2) shouldBe 2
      Cart.applyOffer(Orange, 3) shouldBe 2
      Cart.applyOffer(Orange, 4) shouldBe 3
    }
  }

  "A shop" should {
    "price a list of items taken from a string" in {
      Shop.checkoutTotalFromInput("Apple,Apple,Orange,Apple") shouldBe Success(Price(145))
      Shop.checkoutTotalFromInput("Apple,,,Apple,Orange,,,") shouldBe Success(Price(85))
      Shop.checkoutTotalFromInput("   Apple,,,Apple ,Orange    ,,,") shouldBe Success(Price(85))
      Shop.checkoutTotalFromInput("Apple,Apple,Orange,Apple, Orange, Orange") shouldBe Success(Price(170))
    }

    // FUTURE TODO: tests for the actual stdin/stdout output
  }


}
