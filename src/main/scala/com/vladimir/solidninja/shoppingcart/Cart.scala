package com.vladimir.solidninja.shoppingcart

import scala.io.StdIn
import scala.util.Try


sealed trait Article

case object Apple extends Article

case object Orange extends Article

object Article {
  def unapply(name: String): Option[Article] = name match {
    case "Apple" => Some(Apple)
    case "Orange" => Some(Orange)
    case _ => None
  }
}

case class Price(pence: Int) extends AnyVal {
  override def toString: String = f"£${Math.abs(pence) / 100}.${Math.abs(pence) % 100}%02d"
}

object Cart {

  /**
   * Compute the price of an article in the shop.
   *
   * @note As the list of possible articles is static, do a simple pattern match
   */
  def priceOf(article: Article): Price = article match {
    case Apple => Price(60)
    case Orange => Price(25)
  }

  private[shoppingcart] def groupByCount(articles: Seq[Article]): Map[Article, Int] = articles.groupBy(identity).mapValues(_.length)

  private[shoppingcart] def buyOneGetOneFree(count: Int): Int = count / 2 + count % 2
  private[shoppingcart] def threeForThePriceOfTwo(count: Int) = (count / 3) * 2 + count % 3

  private[shoppingcart] def applyOffer(article: Article, count: Int): Int = article match {
    case Apple => buyOneGetOneFree(count)
    case Orange => threeForThePriceOfTwo(count)
  }

  /**
   * Compute the price of all articles in the cart
   *
   * @note avoiding the Numeric[] typeclass for simplicity without sacrificing type safety
   */
  def checkoutTotal(articles: Seq[Article]): Price = Price(
    groupByCount(articles)
      .map { case (article, count) => applyOffer(article, count) * priceOf(article).pence }
      .sum
  )
}


object Shop extends App {

  /**
   * Convert input to a sequence of articles
   *
   * @note Error handling simply returns the first error, which is simpler than collecting all the errors
   */
  private[Shop] def inputToArticles(input: String): Try[Seq[Article]] = Try {
    val rawStrings = input.split(",").map(_.trim).filterNot(_.isEmpty)
    rawStrings.map {
      case Article(a) => a
      case other => throw new IllegalArgumentException(s"Unknown article name '$other'")
    }
  }

  def checkoutTotalFromInput(input: String): Try[Price] = inputToArticles(input).map(Cart.checkoutTotal)

  Console.println("Please input list of items separated by commas:")
  // TODO: more friendly error handling
  val price = Option(StdIn.readLine()).map(checkoutTotalFromInput).get
  Console.println(price)
}